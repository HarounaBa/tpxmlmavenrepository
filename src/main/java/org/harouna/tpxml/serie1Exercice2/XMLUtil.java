package org.harouna.tpxml.serie1Exercice2;

import java.io.File;
import org.harouna.tpxml.serie1Exercice2.*;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;


public class XMLUtil {


	public Document read(File f) throws DocumentException{
		SAXReader saxReader = new SAXReader();
		
		//saxReader.read(xmlFile);
		Document document = saxReader.read(f); //Ctrl + 1
		return document;
		//Element rootElement = document.getRootElement(); //unique element  racine du xml (getRootElement())
	}
	
	public Marin deserialize(Document document){
		
		Element rootElement = document.getRootElement(); //unique element  racine du xml (getRootElement())

		String nom = rootElement.element("nom").getStringValue();
		String prenom = rootElement.element("prenom").getStringValue();
		String age = rootElement.element("age").getStringValue();
		
		//String nom = rootElement.elementText("nom"); Pareil que ligne 36

		String id = rootElement.attributeValue("id");
		
		Marin m = new Marin();
		
		m.setNom(nom);
		m.setPrenom(prenom);
		m.setAge(Integer.parseInt(age));
		m.setId(Integer.parseInt(id));
		
		return m;
				
		}
	



}
