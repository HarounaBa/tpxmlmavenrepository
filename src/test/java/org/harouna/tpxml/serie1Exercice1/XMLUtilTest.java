package org.harouna.tpxml.serie1Exercice1;

import org.assertj.core.api.Assertions;
import org.dom4j.Document;
import org.junit.Test;

public class XMLUtilTest {
	
	@Test
	/*M�thode de test*/
	public void should_return_marin_when_serialized_is_called_with_a_marin(){
		
		//Given :Cr�er l'environnement
		Marin marin = new Marin("Surcouf", "Robert");
		XMLUtil xmlUtil = new XMLUtil();
		
		//When :Appeler la m�thode
		Document document = xmlUtil.serialize(marin);
		
		//Then :ce qu'on attend
		Assertions.assertThat(document.getRootElement().getName()).isEqualTo("marin");
		Assertions.assertThat(document.getRootElement().element("nom").getText()).isEqualTo("Surcouf");
		Assertions.assertThat(document.getRootElement().element("prenom").getText()).isEqualTo("Robert");

	}
	
	
	
}
